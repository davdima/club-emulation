<?php

use components\Club;
use components\PeopleBehaviour;
use components\TrackList;
use components\objects\People;
use components\objects\Track;
use League\CLImate\CLImate;

require "vendor/autoload.php";

$cli = new CLImate();


$trackList = new TrackList();
$trackList->list = [
  new Track(Track::GENRE_RNB),
  new Track(Track::GENRE_ELECTRO_HOUSE),
  new Track(Track::GENRE_POP),
  new Track(Track::GENRE_RNB),
  new Track(Track::GENRE_ELECTRO_HOUSE),
  new Track(Track::GENRE_RNB),
  new Track(Track::GENRE_POP),
  new Track(Track::GENRE_ELECTRO_HOUSE),
];


$club = new Club(new PeopleBehaviour, $trackList, [
  new People(1, [Track::GENRE_RNB]),
  new People(2, [Track::GENRE_POP]),
  new People(3, [Track::GENRE_POP]),
  new People(4, [Track::GENRE_ELECTRO_HOUSE]),
  new People(5, [Track::GENRE_POP]),
]);

$cli->clear();

while (true) {

  array_map(function ($item) use ($cli) {
    $cli->out($item);
  }, $club->dump());

  sleep(5);
  $cli->clear();
}
