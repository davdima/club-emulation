<?php

namespace components\behaviours;

class GenrePopBehaviour extends AbstractBehaviour
{
  /**
   * @inheritdoc
   */
  public function getAttributes()
  {
    return [
      'плавные движения туловищем',
      'плавные движения руками',
      'плавные движения ногами',
      'плавные движения головой',
    ];
  }

}