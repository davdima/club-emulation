<?php

namespace components\behaviours;

class GenreHipHopBehaviour extends AbstractBehaviour
{
  /**
   * @inheritdoc
   */
  public function getAttributes()
  {
    return [
      'Покачивание телом вперед и назад',
      'Ноги в полуприседе',
      'Руги согнуты в локтях',
      'Голова вперед-назад'
    ];
  }


}