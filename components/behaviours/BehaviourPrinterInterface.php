<?php

namespace components\behaviours;

/**
 * Интерфейс для печати повеведения
 * Interface BehaviourPrinterInterface
 * @package components\behaviours
 */
interface BehaviourPrinterInterface
{
  /**
   * @param AbstractBehaviour $behaviour
   * @return string
   */
  public function print(AbstractBehaviour $behaviour);
}