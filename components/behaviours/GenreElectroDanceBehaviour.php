<?php

namespace components\behaviours;

class GenreElectroDanceBehaviour extends AbstractBehaviour
{

  /**
   * @return array
   */
  public function getAttributes()
  {
    return [
      'покачивание туловищем вперед-назад',
      'почти не движения головой',
      'круговые движения - вращения руками',
      'ноги двигаются в ритме',
    ];
  }

}