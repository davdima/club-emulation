<?php

namespace components\behaviours;

abstract class AbstractBehaviour
{
  /**
   * @return string[]
   */
  abstract public function getAttributes();
}