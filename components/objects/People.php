<?php

namespace components\objects;

class People
{
  /**
   * @var int
   */
  public $id;

  /**
   * @var array[]
   */
  public $genreList = [];

  /**
   * People constructor.
   * @param int $id
   * @param array $genreList
   */
  public function __construct(int $id, array $genreList)
  {
    $this->id = $id;
    $this->genreList = $genreList;
  }


  /**
   * @return string
   */
  public function __toString()
  {
    return sprintf('People: %d; genre list: %s', $this->id, implode(', ', $this->genreList));
  }
}