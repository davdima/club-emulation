<?php

namespace components\objects;

class Track
{

  const GENRE_ELECTRO_HOUSE = 'electro-house';
  const GENRE_POP = 'pop';
  const GENRE_RNB = 'rnb';

  public $genre;

  /**
   * Track constructor.
   * @param string $genre
   */
  public function __construct(string $genre)
  {
    $this->genre = $genre;
  }

  public function __toString()
  {
    return 'Track genre:' . $this->genre;
  }
}