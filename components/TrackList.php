<?php

namespace components;

use components\objects\Track;

class TrackList
{
  /** @var Track[] */
  public $list;

  /**
   * @var Track
   */
  protected $current;

  /**
   * @return Track
   */
  public function getCurrent(): Track
  {
    if ($this->current === null) {
      $this->current = current($this->list);
    }
    return $this->current;
  }

  public function next()
  {
    $this->current = next($this->list);
    if ($this->current === false) {
      $this->current = reset($this->list);
    }
  }
}