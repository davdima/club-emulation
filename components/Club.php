<?php

namespace components;

use components\behaviours\AbstractBehaviour;
use components\behaviours\BehaviourPrinterInterface;
use components\objects\People;

class Club
{
  /**
   * @var TrackList
   */
  protected $trackList;

  /**
   * @var People[]
   */
  protected $people;

  /**
   * @var PeopleBehaviour
   */
  protected $peopleBehaviour;

  /**
   * @var BehaviourPrinterInterface
   */
  protected $behaviourPrinter;

  /**
   * Club constructor.
   * @param PeopleBehaviour $peopleBehaviour
   * @param TrackList $trackList
   * @param array $people
   */
  public function __construct(PeopleBehaviour $peopleBehaviour, TrackList $trackList, array $people)
  {
    $this->peopleBehaviour = $peopleBehaviour;
    $this->trackList = $trackList;
    $this->people = $people;
  }

  /**
   * @return BehaviourPrinterInterface
   */
  public function getBehaviourPrinter(): BehaviourPrinterInterface
  {
    if ($this->behaviourPrinter === null) {
      $this->behaviourPrinter = new DummyBehaviourPrinter;
    }
    return $this->behaviourPrinter;
  }

  /**
   * @param BehaviourPrinterInterface $behaviourPrinter
   * @return Club
   */
  public function setBehaviourPrinter(BehaviourPrinterInterface $behaviourPrinter): Club
  {
    $this->behaviourPrinter = $behaviourPrinter;

    return $this;
  }

  /**
   * @return array
   */
  public function dump()
  {
    $currentTrack = $this->trackList->getCurrent();

    $data = [];
    $data[] = 'Current track';
    $data[] = $currentTrack->__toString();
    $data[] = '';


    foreach ($this->people as $people) {
      $behaviours = array_map(function(AbstractBehaviour $behaviour) {
        return $this->getBehaviourPrinter()->print($behaviour);
      }, $this->peopleBehaviour->getBehaviour($people, $currentTrack));

      $data[] = $people->__toString() . '; ' . implode(', ', $behaviours);
    }

    $this->trackList->next();

    return $data;
  }
}