<?php

namespace components;

use components\behaviours\AbstractBehaviour;
use components\behaviours\BehaviourPrinterInterface;

class DummyBehaviourPrinter implements BehaviourPrinterInterface
{
  /**
   * @param AbstractBehaviour $behaviour
   * @return string
   */
  public function print(AbstractBehaviour $behaviour)
  {
    $attributes = $behaviour->getAttributes();
    if (!count($attributes)) return 'Empty behaviour attributes';

    return $attributes[array_rand($attributes, 1)];
  }

}