<?php

namespace components;

use components\behaviours\GenreElectroDanceBehaviour;
use components\behaviours\GenreHipHopBehaviour;
use components\behaviours\GenrePopBehaviour;
use components\behaviours\NotFoundGenreBahaviour;
use components\objects\People;
use components\objects\Track;

class PeopleBehaviour
{
  /**
   * Это свойство может быть сконфигурированным через конфиги
   *
   * Тут описывается поведение человека от жанра трека
   * @var array
   */
  private $genreBehaviourMap = [
    Track::GENRE_RNB => [GenreHipHopBehaviour::class],
    Track::GENRE_POP => [GenrePopBehaviour::class],
    Track::GENRE_ELECTRO_HOUSE => [GenreElectroDanceBehaviour::class]
  ];

  /**
   * @param People $people
   * @param Track $track
   * @return array
   */
  public function getBehaviour(People $people, Track $track)
  {
    $genre = $track->genre;
    if (!in_array($genre, $people->genreList)) {
      return [new NotFoundGenreBahaviour()];
    }

    $behaviourClasses = $this->genreBehaviourMap[$genre] ?? [NotFoundGenreBahaviour::class];

    return array_map(function($className) {
      return new $className;
    }, $behaviourClasses);
  }
}